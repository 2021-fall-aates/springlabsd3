package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartController {
	
	@Autowired
	private CarpartsService carpartsService;
	
	// Version 1 of getIndex()
	@GetMapping(value = { "/carparts" })
	public String getIndex(Model model) {
		return "carparts";
	}
	
	// Version 2 of getIndex()
//	@GetMapping(value = { "/carparts" })
//	public String getIndex(Model model, String firstName, String lastName) {
//		String fullName = ((firstName != null && firstName.length() > 0)? firstName : "");
//		fullName = ((fullName.length() > 0)? (firstName + " " + lastName) : "");
//		model.addAttribute("fullName", fullName);
//		return "carparts";
//	}

	// Version 1 of getCarpart()
	@GetMapping(value = { "/carparts/other/{partNumber}",
			              "/carparts/engine/{partNumber}",
			              "/carparts/electrical/{partNumber}"})
	public String getCarpart(@PathVariable String partNumber, Model model) {
		return "carparts";
	}
	
	// Version 2 of getCarpart()
//	@GetMapping(value = { "/carparts/other/{partNumber}",
//			              "/carparts/engine/{partNumber}",
//			              "/carparts/electrical/{partNumber}"})
//	public String getCarpart(@PathVariable String partNumber, Model model) {
//        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
//        model.addAttribute("carpart", carpart);
//		return "carparts";
//	}

	
}
