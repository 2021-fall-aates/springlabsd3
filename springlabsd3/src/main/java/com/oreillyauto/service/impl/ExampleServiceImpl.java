package com.oreillyauto.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.ExampleRepository;
import com.oreillyauto.domain.Example;
import com.oreillyauto.service.ExampleService;

@Service
public class ExampleServiceImpl implements ExampleService {

	@Autowired
    ExampleRepository exampleRepo;

    @Override
    public List<Example> getExamples() {
        return exampleRepo.getExamples();
    }

    @Override
    public List<Example> getExampleById(Integer id) {  
         return exampleRepo.getExampleById(id);
    }

	@Override
	@Transactional
	public void testQueries(String day) {
		exampleRepo.testQueries(day);	
	}
	
}
