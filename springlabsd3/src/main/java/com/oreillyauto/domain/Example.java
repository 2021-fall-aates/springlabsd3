package com.oreillyauto.domain;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
//import javax.validation.constraints.NotNull;

//import org.hibernate.validator.constraints.NotEmpty;

@Entity
//@Immutable // <-- Do not add when you have an Auto Incremented GUID
@Table(name="EXAMPLES")
public class Example implements Serializable {

    private static final long serialVersionUID = -5996834027120348470L;

    public Example() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "INTEGER")
    private Integer id;

    //@NotNull
    //@NotEmpty
    @Column(name = "first_name", columnDefinition = "VARCHAR(32)")
    private String firstName;
    
    //@NotNull
    //@NotEmpty
    @Column(name = "last_name", columnDefinition = "VARCHAR(32)")
    private String lastName;

    @Transient
    private String fullname;

    public String getFullname() {
        return firstName + " " + lastName;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Example other = (Example) obj;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Example [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
    }
    
}
